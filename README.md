# MusalaSoftTest

Requisitos:
JDK: 1.8 
Maven: 3.5.4
Revisar que las variables de entorno para Maven y java se encuentren en la variable PATH del sistema, para que así se pueda ejecutar correctamente la aplicación.
Desde la consola de comandos, ubicarse en el directorio donde se encuentra el archivo .jar SpringBootGateways-1.0.jar y ejecutar el siguiente comando:
java -jar SpringBootGateways-1.0.jar
O ejecutar desde la raíz del Proyecto, donde se encuentra el archivo pom.xml, el siguiente comando:
mvn spring-boot:run
La aplicación está configurada para que se ejecute en la siguiente url:
http://localhost:8080/api/gateways (GET, POST)
http://localhost:8080/api/gateways/{gatewayId} (PUT, DELETE)
http://localhost:8080/api/gateways/{gatewayId}/peripheraldevices  (GET, POST)
http://localhost:8080/api/gateways/{gatewayId}/peripheraldevices/{peripheralDeviceId} (PUT, DELETE)

La Base de datos esta configurada in-memory.

Si se desea utilizar MySql usar el siguiente script y descomentar las líneas comentadas en el archivo application.properties de spring boot.

CREATE DATABASE IF NOT EXISTS `gatewaysdb`
USE `gatewaysdb`;

DROP TABLE IF EXISTS `gateway`;
CREATE TABLE IF NOT EXISTS `gateway` (
  `id` varchar(255) NOT NULL,
  `human_readable_name` varchar(255) NOT NULL,
  `ipv4_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `peripheral_device`;
CREATE TABLE IF NOT EXISTS `peripheral_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `status_peripheral_device` int(11) NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `gateway_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1pwpyhvey6ifhf05t6if7h7e` (`gateway_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




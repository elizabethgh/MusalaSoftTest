
-- Dumping database structure for gatewaysdb
DROP DATABASE IF EXISTS `gatewaysdb`;
CREATE DATABASE IF NOT EXISTS `gatewaysdb`;
USE `gatewaysdb`;

-- Dumping structure for table gatewaysdb.gateway
DROP TABLE IF EXISTS `gateway`;
CREATE TABLE IF NOT EXISTS `gateway` (
  `id` varchar(255) NOT NULL,
  `human_readable_name` varchar(255) NOT NULL,
  `ipv4_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gatewaysdb.peripheral_device
DROP TABLE IF EXISTS `peripheral_device`;
CREATE TABLE IF NOT EXISTS `peripheral_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `status_peripheral_device` int(11) NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `gateway_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1pwpyhvey6ifhf05t6if7h7e` (`gateway_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


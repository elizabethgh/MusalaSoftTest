package service.gateways.tests.integrations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import service.gateways.InitialMain;
import service.gateways.entities.GatewayEntity;
import service.gateways.entities.PeripheralDeviceEntity;
import service.gateways.enums.EnumStatus;
import service.gateways.repositories.IGatewayRepository;
import service.gateways.repositories.IPeripheralDeviceRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = InitialMain.class, webEnvironment = DEFINED_PORT)
@ComponentScan(basePackages = {"service.gateways.repositories","service.gateways.entities","service.gateways.api.controllers"})
public class PeripheralDeviceControllerIT {
    private static final String URL_PERIPHERAL_DEVICES = "/gateways/{gatewayId}/peripheraldevices";
    private static final String URL_PERIPHERAL_DEVICES_ID = "/gateways/{gatewayId}/peripheraldevices/{peripheralDeviceId}";

    private static final String JSON_DATA_CREATE = "{\"vendor_device\":\"%s\",\"status_device\":\"%s\"}";
    private static final String JSON_RESPONSE = "{\"message\":\"\",\"success\":true,\"status_response\":%d,\"list_elements\":[%s]}";
    private static final String JSON_RESPONSE_LIST_ELEMENTS = "{\"id_peripheral_device\":%d,\"vendor_device\":\"%s\",\"device_date_created\":\"%s\",\"status_device\":\"%s\"}";

    private static final Integer SIZE_LIST_0 = 0;
    private static final Integer SIZE_LIST_1 = 1;
    private static final Integer SIZE_LIST_2 = 2;

    private static final String GATEWAY_ID_1 = "SN3";
    private static final String GATEWAY_HUMAN_NAME_1 = "Aida Sams Ruiz";
    private static final String GATEWAY_IPV4_1 = "172.65.12.71";

    private static Integer PERIPHERAL_DEVICE_ID_1;
    private static final String PERIPHERAL_DEVICE_VENDOR_1 = "ASUS";
    private static final EnumStatus PERIPHERAL_DEVICE_STATUS_1 = EnumStatus.ONLINE;
    private static final LocalDateTime PERIPHERAL_DEVICE_DATECREATED_1 = LocalDateTime.now();

    private static final String PERIPHERAL_DEVICE_VENDOR_11 = "ASUS OK";
    private static final EnumStatus PERIPHERAL_DEVICE_STATUS_11 = EnumStatus.OFFLINE;

    private static Integer PERIPHERAL_DEVICE_ID_2 = 0;
    private static final String PERIPHERAL_DEVICE_VENDOR_2 = "DELL";
    private static final EnumStatus PERIPHERAL_DEVICE_STATUS_2 = EnumStatus.OFFLINE;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Autowired
    private IGatewayRepository gatewayRepository;

    @Autowired
    private IPeripheralDeviceRepository peripheralDeviceRepository;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();

        saveData();
    }

    @After
    public void tearDown() throws Exception {
        deleteData();
    }

    @Test
    public void getAllPeripheralDevicesByGatewayId() throws Exception {
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.OK.value(), String.format(JSON_RESPONSE_LIST_ELEMENTS, PERIPHERAL_DEVICE_ID_1, PERIPHERAL_DEVICE_VENDOR_1, PERIPHERAL_DEVICE_DATECREATED_1, PERIPHERAL_DEVICE_STATUS_1));
        mockMvc.perform(get(URL_PERIPHERAL_DEVICES, GATEWAY_ID_1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected))
                .andExpect(jsonPath("$.list_elements.*", hasSize(SIZE_LIST_1)))
                .andExpect(jsonPath("$.list_elements[0].id_peripheral_device", equalTo(PERIPHERAL_DEVICE_ID_1)))
                .andExpect(jsonPath("$.list_elements[0].vendor_device", equalTo(PERIPHERAL_DEVICE_VENDOR_1)))
                .andExpect(jsonPath("$.list_elements[0].device_date_created").value(equalTo(PERIPHERAL_DEVICE_DATECREATED_1.toString())))
                .andExpect(jsonPath("$.list_elements[0].status_device").value(equalTo(PERIPHERAL_DEVICE_STATUS_1.toString())));
    }

    @Test
    public void createPeripheralDevice() throws Exception {
        PERIPHERAL_DEVICE_ID_2 = PERIPHERAL_DEVICE_ID_1 + 1;
        String jsonCreate = String.format(JSON_DATA_CREATE, PERIPHERAL_DEVICE_VENDOR_2, PERIPHERAL_DEVICE_STATUS_2);
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.CREATED.value(), PERIPHERAL_DEVICE_ID_2);

        mockMvc.perform(post(URL_PERIPHERAL_DEVICES, GATEWAY_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonCreate))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));

        List<PeripheralDeviceEntity> byGatewayId = peripheralDeviceRepository.findByGatewayId(GATEWAY_ID_1);
        assertThat(byGatewayId, hasSize(SIZE_LIST_2));

        String formatDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        PeripheralDeviceEntity peripheralDeviceEntity = byGatewayId.get(1);
        boolean conditions = peripheralDeviceEntity.getId() == PERIPHERAL_DEVICE_ID_2
                && peripheralDeviceEntity.getVendor().equals(PERIPHERAL_DEVICE_VENDOR_2)
                && peripheralDeviceEntity.getStatus() == PERIPHERAL_DEVICE_STATUS_2
                && peripheralDeviceEntity.getGateway().getId().equals(GATEWAY_ID_1)
                && peripheralDeviceEntity.getDateCreated().compareTo(LocalDateTime.now()) < 0
                && peripheralDeviceEntity.getDateCreated().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).equals(formatDateTime);
        assertTrue(conditions);
    }

    @Test
    public void updatePeripheralDevice() throws Exception {
        String json = String.format(JSON_DATA_CREATE, PERIPHERAL_DEVICE_VENDOR_11, PERIPHERAL_DEVICE_STATUS_11);
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.OK.value(), "");

        mockMvc.perform(put(URL_PERIPHERAL_DEVICES_ID, GATEWAY_ID_1, PERIPHERAL_DEVICE_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));

        Optional<PeripheralDeviceEntity> byId = peripheralDeviceRepository.findById(PERIPHERAL_DEVICE_ID_1);
        assertTrue(byId.isPresent());
        PeripheralDeviceEntity peripheralDeviceEntity = byId.get();
        boolean conditions = peripheralDeviceEntity.getVendor().equals(PERIPHERAL_DEVICE_VENDOR_11)
                && peripheralDeviceEntity.getStatus().equals(PERIPHERAL_DEVICE_STATUS_11)
                && peripheralDeviceEntity.getGateway().getId().equals(GATEWAY_ID_1);
        assertTrue(conditions);
    }

    @Test
    public void deletePeripheralDevice() throws Exception {
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.OK.value(), "");
        mockMvc.perform(delete(URL_PERIPHERAL_DEVICES_ID, GATEWAY_ID_1, PERIPHERAL_DEVICE_ID_1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));

        Optional<PeripheralDeviceEntity> byId = peripheralDeviceRepository.findById(PERIPHERAL_DEVICE_ID_1);
        assertTrue(!byId.isPresent());

        List<PeripheralDeviceEntity> byGatewayId = peripheralDeviceRepository.findByGatewayId(GATEWAY_ID_1);
        assertThat(byGatewayId, hasSize(SIZE_LIST_0));
    }

    private void saveData() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_1, GATEWAY_HUMAN_NAME_1, GATEWAY_IPV4_1);
        gatewayRepository.save(gatewayEntity);

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity(PERIPHERAL_DEVICE_VENDOR_1, PERIPHERAL_DEVICE_DATECREATED_1, PERIPHERAL_DEVICE_STATUS_1, gatewayEntity);
        PeripheralDeviceEntity save = peripheralDeviceRepository.save(peripheralDeviceEntity);
        PERIPHERAL_DEVICE_ID_1 = save.getId();
    }

    private void deleteData() throws Exception {
        Integer arrIds[] = new Integer[2];
        arrIds[0] = PERIPHERAL_DEVICE_ID_1;
        arrIds[1] = PERIPHERAL_DEVICE_ID_2;
        for (Integer id : arrIds) {
            if(peripheralDeviceRepository.findById(id).isPresent())
                peripheralDeviceRepository.deleteById(id);
        }

        if(gatewayRepository.findById(GATEWAY_ID_1).isPresent())
            gatewayRepository.deleteById(GATEWAY_ID_1);
    }
}

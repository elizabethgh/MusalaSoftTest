package service.gateways.tests.integrations;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import service.gateways.InitialMain;
import service.gateways.entities.GatewayEntity;
import service.gateways.repositories.IGatewayRepository;

import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = InitialMain.class, webEnvironment = DEFINED_PORT)
@ComponentScan(basePackages = {"service.gateways.repositories","service.gateways.entities","service.gateways.api.controllers"})
public class GatewayControllerIT {

    private static final String URL_GATEWAYS = "/gateways";
    private static final String URL_GATEWAYS_ID = "/gateways/{gatewayId}";

    private static final String GATEWAY_ID_1 = "SN1";
    private static final String GATEWAY_HUMAN_NAME_1 = "Luis Manuel Lopez";
    private static final String GATEWAY_IPV4_1 = "172.23.45.61";

    private static final String GATEWAY_HUMAN_NAME_11 = "Luis Manuel Lopez Garcia";
    private static final String GATEWAY_IPV4_11 = "172.23.45.62";

    private static final String GATEWAY_ID_2 = "SN2";
    private static final String GATEWAY_HUMAN_NAME_2 = "Rafael Perez";
    private static final String GATEWAY_IPV4_2 = "192.78.12.34";

    private static final String JSON_DATA_CREATE = "{\"id_gateway\":\"%s\",\"human_name\":\"%s\",\"address_ipv4\":\"%s\"}";
    private static final String JSON_DATA_UPDATE = "{\"human_name\":\"%s\",\"address_ipv4\":\"%s\"}";
    private static final String JSON_RESPONSE = "{\"message\":\"\",\"success\":true,\"status_response\":%d,\"list_elements\":[%s]}";
    private static final String JSON_RESPONSE_LIST_ELEMENTS = "{\"id_gateway\":\"%s\",\"human_name\":\"%s\",\"address_ipv4\":\"%s\",\"peripheral_devices\":[]}";

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Autowired
    private IGatewayRepository gatewayRepository;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();

        saveData();
    }

    @After
    public void tearDown() throws Exception {
        deleteData();
    }

    @Test
    public void getAllGateways() throws Exception {
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.OK.value(), String.format(JSON_RESPONSE_LIST_ELEMENTS, GATEWAY_ID_1, GATEWAY_HUMAN_NAME_1, GATEWAY_IPV4_1));
        mockMvc.perform(get(URL_GATEWAYS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected))
                .andExpect(jsonPath("$.list_elements.*", hasSize(1)))
                .andExpect(jsonPath("$.list_elements[0].id_gateway", equalTo(GATEWAY_ID_1)))
                .andExpect(jsonPath("$.list_elements[0].human_name", equalTo(GATEWAY_HUMAN_NAME_1)))
                .andExpect(jsonPath("$.list_elements[0].address_ipv4", equalTo(GATEWAY_IPV4_1)));
    }

    @Test
    public void getGatewayById() throws Exception {
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.FOUND.value(), String.format(JSON_RESPONSE_LIST_ELEMENTS, GATEWAY_ID_1, GATEWAY_HUMAN_NAME_1, GATEWAY_IPV4_1));
        mockMvc.perform(get(URL_GATEWAYS_ID, GATEWAY_ID_1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));
    }

    @Test
    public void createGateway() throws Exception {
        String jsonCreate = String.format(JSON_DATA_CREATE, GATEWAY_ID_2, GATEWAY_HUMAN_NAME_2, GATEWAY_IPV4_2);
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.CREATED.value(), "\"" + GATEWAY_ID_2 + "\"");

        mockMvc.perform(post(URL_GATEWAYS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonCreate))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));

        Optional<GatewayEntity> byId = gatewayRepository.findById(GATEWAY_ID_2);
        assertTrue(byId.isPresent());
        assertTrue(byId.get().getId().equals(GATEWAY_ID_2) && byId.get().getHumanReadableName().equals(GATEWAY_HUMAN_NAME_2) && byId.get().getIpv4Address().equals(GATEWAY_IPV4_2));
    }

    @Test
    public void updateGateway() throws Exception {
        String json = String.format(JSON_DATA_UPDATE, GATEWAY_HUMAN_NAME_11, GATEWAY_IPV4_11);
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.OK.value(), "");

        mockMvc.perform(put(URL_GATEWAYS_ID, GATEWAY_ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));

        Optional<GatewayEntity> byId = gatewayRepository.findById(GATEWAY_ID_1);
        assertTrue(byId.isPresent());
        assertTrue(byId.get().getHumanReadableName().equals(GATEWAY_HUMAN_NAME_11) && byId.get().getIpv4Address().equals(GATEWAY_IPV4_11));
    }

    @Test
    public void deleteGateway() throws Exception {
        String jsonExpected = String.format(JSON_RESPONSE, HttpStatus.OK.value(), "");
        mockMvc.perform(delete(URL_GATEWAYS_ID, GATEWAY_ID_1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(jsonExpected));

        Optional<GatewayEntity> byId = gatewayRepository.findById(GATEWAY_ID_1);
        Assert.assertTrue(!byId.isPresent());
    }

    private void saveData() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_1, GATEWAY_HUMAN_NAME_1, GATEWAY_IPV4_1);
        gatewayRepository.save(gatewayEntity);
    }

    private void deleteData() throws Exception {
        String arrIds[] = new String[2];
        arrIds[0] = GATEWAY_ID_1;
        arrIds[1] = GATEWAY_ID_2;
        for (String id : arrIds) {
            if(gatewayRepository.findById(id).isPresent())
                gatewayRepository.deleteById(id);
        }
    }
}
package service.gateways.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import service.gateways.InitialMain;
import service.gateways.entities.GatewayEntity;
import service.gateways.entities.PeripheralDeviceEntity;
import service.gateways.enums.EnumStatus;
import service.gateways.exceptions.ExceedMaxDevicesException;
import service.gateways.exceptions.ResourceNotFoundException;
import service.gateways.repositories.IGatewayRepository;
import service.gateways.repositories.IPeripheralDeviceRepository;
import service.gateways.services.IPeripheralDeviceService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static service.gateways.messages.Messages.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InitialMain.class)
@ComponentScan(basePackages = {"service.gateways.repositories","service.gateways.entities", "service.gateways.services"})
public class PeripheralDeviceServiceTest {
    private static final String GATEWAY_ID_1 = "SN6";
    private static final String GATEWAY_HUMAN_NAME_1 = "Hansel Zall Fons";
    private static final String GATEWAY_IPV4_1 = "92.13.55.254";

    private static final String GATEWAY_ID_2 = "SN8";
    private static final String GATEWAY_HUMAN_NAME_2 = "Maria Kars";
    private static final String GATEWAY_IPV4_2 = "192.23.67.222";

    private static final String GATEWAY_ID_3 = "SN81";

    private static Integer PERIPHERAL_DEVICE_ID_1;
    private static final String PERIPHERAL_DEVICE_VENDOR_1 = "ASUS NEW";
    private static final EnumStatus PERIPHERAL_DEVICE_STATUS_1 = EnumStatus.ONLINE;
    private static final LocalDateTime PERIPHERAL_DEVICE_DATECREATED_1 = LocalDateTime.now();

    private static final String PERIPHERAL_DEVICE_VENDOR_2 = "TOSHIBA";
    private static final EnumStatus PERIPHERAL_DEVICE_STATUS_2 = EnumStatus.ONLINE;

    private static Integer PERIPHERAL_DEVICE_ID_3 = 99;

    private static final Integer SIZE_LIST_1 = 1;

    @Autowired
    private IGatewayRepository gatewayRepository;

    @Autowired
    private IPeripheralDeviceRepository peripheralDeviceRepository;

    @Autowired
    private IPeripheralDeviceService peripheralDeviceService;

    @Before
    public void setUp() throws Exception {
        saveData();
    }

    @After
    public void tearDown() throws Exception {
        deleteData();
    }

    @Test
    public void save_ok() throws Exception {
        Integer PERIPHERAL_DEVICE_ID_2 = PERIPHERAL_DEVICE_ID_1 + 1;
        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        PeripheralDeviceEntity save = peripheralDeviceService.save(GATEWAY_ID_1, peripheralDeviceEntity);

        String formatDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        boolean conditions = save.getId() == PERIPHERAL_DEVICE_ID_2
                && save.getVendor().equals(PERIPHERAL_DEVICE_VENDOR_2)
                && save.getStatus() == PERIPHERAL_DEVICE_STATUS_2
                && save.getGateway().getId().equals(GATEWAY_ID_1)
                && save.getDateCreated().compareTo(LocalDateTime.now()) < 0
                && save.getDateCreated().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).equals(formatDateTime);
        assertTrue(conditions);
    }

    @Rule
    public ExpectedException exceptionRule1 = ExpectedException.none();

    @Test
    public void save_resource_not_found_exception() throws Exception {
        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(String.format(message_gateway_not_found, GATEWAY_ID_2));

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        peripheralDeviceService.save(GATEWAY_ID_2, peripheralDeviceEntity);
    }

    @Test
    public void save_exceed_max_devices_exception() throws Exception {
        insertDataArray();

        exceptionRule1.expect(ExceedMaxDevicesException.class);
        exceptionRule1.expectMessage(String.format(message_peripheral_devices_count, max_peripheral_devices_count));

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        peripheralDeviceService.save(GATEWAY_ID_1, peripheralDeviceEntity);
    }

    @Test
    public void update_ok() throws Exception {
        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        peripheralDeviceService.update(GATEWAY_ID_1, PERIPHERAL_DEVICE_ID_1, peripheralDeviceEntity);

        Optional<PeripheralDeviceEntity> byId = peripheralDeviceRepository.findById(PERIPHERAL_DEVICE_ID_1);
        assertTrue(byId.isPresent());
        PeripheralDeviceEntity save = byId.get();
        boolean conditions = save.getId() == PERIPHERAL_DEVICE_ID_1
                && save.getVendor().equals(PERIPHERAL_DEVICE_VENDOR_2)
                && save.getStatus() == PERIPHERAL_DEVICE_STATUS_2
                && save.getGateway().getId().equals(GATEWAY_ID_1);
        assertTrue(conditions);
    }

    @Test
    public void update_resource_not_found_exception_gateway() throws Exception {
        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(String.format(message_gateway_not_found, GATEWAY_ID_3));

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        peripheralDeviceService.update(GATEWAY_ID_3, PERIPHERAL_DEVICE_ID_1, peripheralDeviceEntity);
    }

    @Test
    public void update_resource_not_found_exception_peripheral() throws Exception {
        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(String.format(message_peripheral_device_not_found, PERIPHERAL_DEVICE_ID_3));

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        peripheralDeviceService.update(GATEWAY_ID_1, PERIPHERAL_DEVICE_ID_3, peripheralDeviceEntity);
    }

    @Test
    public void update_resource_not_found_exception_gateway_not_same() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_2, GATEWAY_HUMAN_NAME_2, GATEWAY_IPV4_2);
        gatewayRepository.save(gatewayEntity);

        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(message_peripheral_devices_has_not_the_same_gateway);

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
        peripheralDeviceEntity.setStatus(PERIPHERAL_DEVICE_STATUS_2);
        peripheralDeviceEntity.setVendor(PERIPHERAL_DEVICE_VENDOR_2);
        peripheralDeviceService.update(GATEWAY_ID_2, PERIPHERAL_DEVICE_ID_1, peripheralDeviceEntity);
    }

    @Test
    public void delete_ok() throws Exception {
        peripheralDeviceService.delete(GATEWAY_ID_1, PERIPHERAL_DEVICE_ID_1);

        Optional<PeripheralDeviceEntity> byId = peripheralDeviceRepository.findById(PERIPHERAL_DEVICE_ID_1);
        assertTrue(!byId.isPresent());
    }

    @Test
    public void delete_resource_not_found_exception_gateway() throws Exception {
        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(String.format(message_gateway_not_found, GATEWAY_ID_2));

        peripheralDeviceService.delete(GATEWAY_ID_2, PERIPHERAL_DEVICE_ID_1);
    }

    @Test
    public void delete_resource_not_found_exception_peripheral() throws Exception {
        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(String.format(message_peripheral_device_not_found, PERIPHERAL_DEVICE_ID_3));

        peripheralDeviceService.delete(GATEWAY_ID_1, PERIPHERAL_DEVICE_ID_3);
    }

    @Test
    public void delete_resource_not_found_exception_gateway_not_same() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_2, GATEWAY_HUMAN_NAME_2, GATEWAY_IPV4_2);
        gatewayRepository.save(gatewayEntity);

        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(message_peripheral_devices_has_not_the_same_gateway);

        peripheralDeviceService.delete(GATEWAY_ID_2, PERIPHERAL_DEVICE_ID_1);
    }

    @Test
    public void findAllByGatewayId_ok() throws Exception {
        List<PeripheralDeviceEntity> allByGatewayId = peripheralDeviceService.findAllByGatewayId(GATEWAY_ID_1);
        assertThat(allByGatewayId, hasSize(SIZE_LIST_1));
    }

    @Test
    public void findAllByGatewayId_resource_not_found_exception() throws Exception {
        exceptionRule1.expect(ResourceNotFoundException.class);
        exceptionRule1.expectMessage(String.format(message_gateway_not_found, GATEWAY_ID_2));

        peripheralDeviceService.findAllByGatewayId(GATEWAY_ID_2);
    }

    private void saveData() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_1, GATEWAY_HUMAN_NAME_1, GATEWAY_IPV4_1);
        gatewayRepository.save(gatewayEntity);

        PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity(PERIPHERAL_DEVICE_VENDOR_1, PERIPHERAL_DEVICE_DATECREATED_1, PERIPHERAL_DEVICE_STATUS_1, gatewayEntity);
        PeripheralDeviceEntity save = peripheralDeviceRepository.save(peripheralDeviceEntity);
        PERIPHERAL_DEVICE_ID_1 = save.getId();
    }

    private void deleteData() throws Exception {
        for (int i = 1; i < 15; i++) {
            if(peripheralDeviceRepository.findById(i).isPresent())
                peripheralDeviceRepository.deleteById(i);
        }

        if(gatewayRepository.findById(GATEWAY_ID_1).isPresent())
            gatewayRepository.deleteById(GATEWAY_ID_1);
    }

    private void insertDataArray()
    {
        Optional<GatewayEntity> byId = gatewayRepository.findById(GATEWAY_ID_1);
        for (int i = 0; i < 9; i++) {
            if(byId.isPresent()) {
                PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity(PERIPHERAL_DEVICE_VENDOR_1 + i, LocalDateTime.now(), PERIPHERAL_DEVICE_STATUS_1, byId.get());
                peripheralDeviceRepository.save(peripheralDeviceEntity);
            }
        }
    }
}
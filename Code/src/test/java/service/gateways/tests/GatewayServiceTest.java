package service.gateways.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import service.gateways.InitialMain;
import service.gateways.entities.GatewayEntity;
import service.gateways.exceptions.AddressIpv4Exception;
import service.gateways.exceptions.ResourceExistsException;
import service.gateways.exceptions.ResourceNotFoundException;
import service.gateways.messages.Messages;
import service.gateways.repositories.IGatewayRepository;
import service.gateways.services.IGatewayService;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InitialMain.class)
@ComponentScan(basePackages = {"service.gateways.repositories","service.gateways.entities", "service.gateways.services"})
public class GatewayServiceTest {
    @Autowired
    private IGatewayService gatewayService;

    @Autowired
    private IGatewayRepository gatewayRepository;

    private static final String GATEWAY_ID_1 = "SN4";
    private static final String GATEWAY_HUMAN_NAME_1 = "Raisa Hills";
    private static final String GATEWAY_IPV4_1 = "172.21.37.89";

    private static final String GATEWAY_ID_2 = "SN5";
    private static final String GATEWAY_HUMAN_NAME_2 = "Jose Sanchez";
    private static final String GATEWAY_IPV4_2 = "142.34.56.91";

    private static final String GATEWAY_IPV4_WRONG = "172.21.AA.C";

    @Before
    public void setUp() throws Exception {
        saveData();
    }

    @After
    public void tearDown() throws Exception {
        deleteData();
    }

    @Test
    public void save_ok() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_2, GATEWAY_HUMAN_NAME_2, GATEWAY_IPV4_2);
        GatewayEntity save = gatewayService.save(gatewayEntity);
        boolean conditions = save.getId().equals(GATEWAY_ID_2) && save.getHumanReadableName().equals(GATEWAY_HUMAN_NAME_2) && save.getIpv4Address().equals(GATEWAY_IPV4_2);
        assertTrue(conditions);

        Optional<GatewayEntity> byId = gatewayRepository.findById(GATEWAY_ID_2);
        assertTrue(byId.isPresent());
        save = byId.get();
        conditions = save.getId().equals(GATEWAY_ID_2) && save.getHumanReadableName().equals(GATEWAY_HUMAN_NAME_2) && save.getIpv4Address().equals(GATEWAY_IPV4_2);
        assertTrue(conditions);

        gatewayRepository.deleteById(GATEWAY_ID_2);
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    //@Test(expected = ResourceExistsException.class)
    public void save_resource_exists_exception() throws Exception {
        exceptionRule.expect(ResourceExistsException.class);
        exceptionRule.expectMessage(Messages.message_gateway_exists);
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_1, GATEWAY_HUMAN_NAME_2, GATEWAY_IPV4_2);
        gatewayService.save(gatewayEntity);
    }

    @Test
    //@Test(expected = AddressIpv4Exception.class)
    public void save_address_ipv4_exception() throws Exception {
        exceptionRule.expect(AddressIpv4Exception.class);
        exceptionRule.expectMessage(Messages.message_address_not_valid);
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_2, GATEWAY_HUMAN_NAME_2, GATEWAY_IPV4_WRONG);
        gatewayService.save(gatewayEntity);
    }

    @Test
    public void update_ok() throws Exception {
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setHumanReadableName(GATEWAY_HUMAN_NAME_2);
        gatewayEntity.setIpv4Address(GATEWAY_IPV4_2);
        gatewayService.update(GATEWAY_ID_1, gatewayEntity);

        Optional<GatewayEntity> byId = gatewayRepository.findById(GATEWAY_ID_1);
        assertTrue(byId.isPresent());
        GatewayEntity gatewayUpdated = byId.get();
        boolean conditions = gatewayUpdated.getHumanReadableName().equals(GATEWAY_HUMAN_NAME_2) && gatewayUpdated.getIpv4Address().equals(GATEWAY_IPV4_2);
        assertTrue(conditions);
    }

    @Test
    //@Test(expected = ResourceNotFoundException.class)
    public void update_resource_not_found_exception() throws Exception {
        exceptionRule.expect(ResourceNotFoundException.class);
        exceptionRule.expectMessage(String.format(Messages.message_gateway_not_found, GATEWAY_ID_2));
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setHumanReadableName(GATEWAY_HUMAN_NAME_2);
        gatewayEntity.setIpv4Address(GATEWAY_IPV4_2);
        gatewayService.update(GATEWAY_ID_2, gatewayEntity);
    }

    @Test
    //@Test(expected = AddressIpv4Exception.class)
    public void update_address_ipv4_exception() throws Exception {
        exceptionRule.expect(AddressIpv4Exception.class);
        exceptionRule.expectMessage(Messages.message_address_not_valid);
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setHumanReadableName(GATEWAY_HUMAN_NAME_2);
        gatewayEntity.setIpv4Address(GATEWAY_IPV4_WRONG);
        gatewayService.update(GATEWAY_ID_1, gatewayEntity);
    }

    @Test
    public void delete_ok() throws Exception {
        gatewayService.delete(GATEWAY_ID_1);
    }

    @Test
    //@Test(expected = ResourceNotFoundException.class)
    public void delete_resource_not_found_exception() throws Exception {
        exceptionRule.expect(ResourceNotFoundException.class);
        exceptionRule.expectMessage(String.format(Messages.message_gateway_not_found, GATEWAY_ID_2));
        gatewayService.delete(GATEWAY_ID_2);
    }

    @Test
    public void findAll() throws Exception {
        List<GatewayEntity> all = gatewayService.findAll();
        assertThat(all, hasSize(1));
    }

    @Test
    public void findById() throws Exception {
        GatewayEntity byId = gatewayService.findById(GATEWAY_ID_1);
        boolean conditions = byId.getId().equals(GATEWAY_ID_1) && byId.getHumanReadableName().equals(GATEWAY_HUMAN_NAME_1) && byId.getIpv4Address().equals(GATEWAY_IPV4_1);
        assertTrue(conditions);
    }

    @Test
    //@Test(expected = ResourceNotFoundException.class)
    public void findById_resource_not_found_exception() throws Exception {
        exceptionRule.expect(ResourceNotFoundException.class);
        exceptionRule.expectMessage(String.format(Messages.message_gateway_not_found, GATEWAY_ID_2));
        gatewayService.findById(GATEWAY_ID_2);
    }

    private void saveData() {
        GatewayEntity gatewayEntity = new GatewayEntity(GATEWAY_ID_1, GATEWAY_HUMAN_NAME_1, GATEWAY_IPV4_1);
        gatewayRepository.save(gatewayEntity);
    }

    private void deleteData() {
        String arrIds[] = new String[2];
        arrIds[0] = GATEWAY_ID_1;
        arrIds[1] = GATEWAY_ID_2;
        for (String id : arrIds) {
            if(gatewayRepository.findById(id).isPresent())
                gatewayRepository.deleteById(id);
        }
    }
}
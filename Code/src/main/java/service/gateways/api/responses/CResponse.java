package service.gateways.api.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CResponse implements Serializable
{
    protected String message = "";

    protected Boolean success = true;

    @JsonProperty("status_response")
    protected Integer status = HttpStatus.INTERNAL_SERVER_ERROR.value();

    @JsonProperty("list_elements")
    protected List listElements = new ArrayList();

    public Boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(Boolean success)
    {
        this.success = success;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public List getListElements()
    {
        return listElements;
    }

    public void setListElements(List listElements)
    {
        this.listElements = listElements;
    }

    public static void setResponse(CResponse response, String message, boolean success, int status, List list) {
        if(response == null) return;
        response.setSuccess(success);
        response.setStatus(status);
        response.setMessage(message);
        if(list != null) response.setListElements(list);
    }
}


package service.gateways.api.controllers;

import service.gateways.exceptions.AddressIpv4Exception;
import service.gateways.exceptions.ResourceExistsException;
import service.gateways.api.responses.CResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.gateways.entities.GatewayEntity;
import service.gateways.exceptions.ResourceNotFoundException;
import service.gateways.services.IGatewayService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static service.gateways.api.responses.CResponse.setResponse;

@RestController
@RequestMapping(path = "gateways", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class GatewayController {
    private IGatewayService gatewayService;

    @Autowired
    public GatewayController(IGatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CResponse> getAllGateways() {
        CResponse response = new CResponse();
        try {
            List<GatewayEntity> all = gatewayService.findAll();
            setResponse(response, "", true, HttpStatus.OK.value(), all);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @GetMapping(value = "{gatewayId}")
    @ResponseStatus(HttpStatus.FOUND)
    public ResponseEntity<CResponse> getGatewayById(@PathVariable String gatewayId) {
        CResponse response = new CResponse();
        try {
            GatewayEntity gatewayEntity = gatewayService.findById(gatewayId);
            ArrayList<GatewayEntity> arr = new ArrayList<>();
            arr.add(gatewayEntity);
            setResponse(response, "", true, HttpStatus.FOUND.value(), arr);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CResponse> createGateway(@Valid @RequestBody GatewayEntity gatewayRequest) {
        CResponse response = new CResponse();
        try {
            GatewayEntity save = gatewayService.save(gatewayRequest);
            ArrayList<String> arr = new ArrayList<>();
            arr.add(save.getId());
            setResponse(response, "", true, HttpStatus.CREATED.value(), arr);
        }
        catch (ResourceExistsException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.CONFLICT.value(), null);
        }
        catch (AddressIpv4Exception ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_ACCEPTABLE.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @PutMapping(value = "{gatewayId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CResponse> updateGateway(@PathVariable String gatewayId,
                                                   @Valid @RequestBody GatewayEntity gatewayRequest) {
        CResponse response = new CResponse();
        try {
            gatewayService.update(gatewayId, gatewayRequest);
            setResponse(response, "", true, HttpStatus.OK.value(), null);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (AddressIpv4Exception ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_ACCEPTABLE.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @DeleteMapping(value = "{gatewayId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CResponse> deleteGateway(@PathVariable String gatewayId) {
        CResponse response = new CResponse();
        try {
            gatewayService.delete(gatewayId);
            setResponse(response, "", true, HttpStatus.OK.value(), null);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }
}
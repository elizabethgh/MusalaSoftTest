package service.gateways.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.gateways.api.responses.CResponse;
import service.gateways.entities.PeripheralDeviceEntity;
import service.gateways.exceptions.ExceedMaxDevicesException;
import service.gateways.exceptions.ResourceNotFoundException;
import service.gateways.services.IPeripheralDeviceService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static service.gateways.api.responses.CResponse.setResponse;

@RestController
@RequestMapping(path = "gateways/{gatewayId}/peripheraldevices", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class PeripheralDeviceController {

    private final IPeripheralDeviceService peripheralDeviceService;

    @Autowired
    public PeripheralDeviceController(IPeripheralDeviceService peripheralDeviceService) {
        this.peripheralDeviceService = peripheralDeviceService;
    }

    @GetMapping
    public ResponseEntity<CResponse> getAllPeripheralDevicesByGatewayId(@PathVariable(value = "gatewayId") String gatewayId) {
        CResponse response = new CResponse();
        try {
            List<PeripheralDeviceEntity> all = peripheralDeviceService.findAllByGatewayId(gatewayId);
            setResponse(response, "", true, HttpStatus.OK.value(), all);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @PostMapping
    public ResponseEntity<CResponse> createPeripheralDevice(@PathVariable(value = "gatewayId") String gatewayId,
                                                            @Valid @RequestBody PeripheralDeviceEntity peripheralDeviceRequest) {
        CResponse response = new CResponse();
        try {
            PeripheralDeviceEntity save = peripheralDeviceService.save(gatewayId, peripheralDeviceRequest);
            ArrayList<Integer> arr = new ArrayList<>();
            arr.add(save.getId());
            setResponse(response, "", true, HttpStatus.CREATED.value(), arr);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (ExceedMaxDevicesException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_ACCEPTABLE.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @PutMapping(value = "{peripheralDeviceId}")
    public ResponseEntity<CResponse> updatePeripheralDevice(@PathVariable(value = "gatewayId") String gatewayId,
                                                            @PathVariable(value = "peripheralDeviceId") Integer peripheralDeviceId,
                                                            @Valid @RequestBody PeripheralDeviceEntity peripheralDeviceRequest) {
        CResponse response = new CResponse();
        try {
            peripheralDeviceService.update(gatewayId, peripheralDeviceId, peripheralDeviceRequest);
            setResponse(response, "", true, HttpStatus.OK.value(), null);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @DeleteMapping(value = "{peripheralDeviceId}")
    public ResponseEntity<CResponse> deletePeripheralDevice(@PathVariable(value = "gatewayId") String gatewayId,
                                                            @PathVariable(value = "peripheralDeviceId") Integer peripheralDeviceId) {
        CResponse response = new CResponse();
        try {
            peripheralDeviceService.delete(gatewayId, peripheralDeviceId);
            setResponse(response, "", true, HttpStatus.OK.value(), null);
        }
        catch (ResourceNotFoundException ex) {
            setResponse(response, ex.getMessage(), true, HttpStatus.NOT_FOUND.value(), null);
        }
        catch (Exception ex) {
            setResponse(response, ex.getMessage(), false, HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        return ResponseEntity.status(response.getStatus()).body(response);
    }
}

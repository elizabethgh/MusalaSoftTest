package service.gateways.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import service.gateways.utils.AddressValidator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "gateway")
public class GatewayEntity {

    @Id
    @JsonProperty("id_gateway")
    private String id;

    @Column(name = "human_readable_name", nullable = false)
    @NotNull
    @NotEmpty
    @NotBlank
    @JsonProperty("human_name")
    private String humanReadableName;

    @Column(name = "ipv4_address", nullable = false)
    @NotNull
    @NotEmpty
    @NotBlank
    //@Pattern(regexp = AddressValidator.IPV4_EXP, message = Messages.message_address_not_valid)
    @JsonProperty("address_ipv4")
    private String ipv4Address;

    @OneToMany(mappedBy = "gateway", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JsonProperty("peripheral_devices")
    private Collection<PeripheralDeviceEntity> peripheralDevices = new ArrayList<>();

    public GatewayEntity(){}

    public GatewayEntity(@NotNull String id, @NotNull String humanReadableName, @NotNull String ipv4Address) {
        this.id = id;
        this.humanReadableName = humanReadableName;
        this.ipv4Address = ipv4Address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHumanReadableName() {
        return humanReadableName;
    }

    public void setHumanReadableName(String humanReadableName) {
        this.humanReadableName = humanReadableName;
    }

    public String getIpv4Address() {
        return ipv4Address;
    }

    public void setIpv4Address(String ipv4Address) {
        this.ipv4Address = ipv4Address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GatewayEntity that = (GatewayEntity) o;

        return (id != null ? id.equals(that.id) : that.id == null) && (humanReadableName != null ? humanReadableName.equals(that.humanReadableName) : that.humanReadableName == null) && (ipv4Address != null ? ipv4Address.equals(that.ipv4Address) : that.ipv4Address == null);
    }

    @Override
    public int hashCode()
    {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (humanReadableName != null ? humanReadableName.hashCode() : 0);
        result = 31 * result + (ipv4Address != null ? ipv4Address.hashCode() : 0);
        return result;
    }

    public Collection<PeripheralDeviceEntity> getPeripheralDevices()
    {
        return peripheralDevices;
    }

    public void setPeripheralDevices(Collection<PeripheralDeviceEntity> peripheralDevices)
    {
        this.peripheralDevices = peripheralDevices;
    }

    @JsonIgnore
    public boolean isValidAddressIpv4() {
        return AddressValidator.validate(ipv4Address);
    }
}



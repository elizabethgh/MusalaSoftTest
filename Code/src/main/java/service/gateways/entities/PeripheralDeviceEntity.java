package service.gateways.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import service.gateways.enums.EnumStatus;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "peripheral_device")
public class PeripheralDeviceEntity  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id_peripheral_device")
    private int id;

    @Column(name = "vendor", nullable = false)
    @NotNull
    @NotEmpty
    @NotBlank
    @JsonProperty("vendor_device")
    private String vendor;

    @Column(name = "date_created", nullable = false)
    @JsonProperty("device_date_created")
    private LocalDateTime dateCreated;

    @Column(name = "status_peripheral_device", nullable = false)
    @NotNull
    @JsonProperty("status_device")
    private EnumStatus status;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "gateway_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private GatewayEntity gateway;

    public PeripheralDeviceEntity(){}

    public PeripheralDeviceEntity(@NotNull String vendor, LocalDateTime dateCreated, @NotNull EnumStatus status, GatewayEntity gateway) {
        this.vendor = vendor;
        this.dateCreated = dateCreated;
        this.status = status;
        this.gateway = gateway;
    }

    public int getId() {
        return id;
    }

    public void setId(int uuid) {
        this.id = id;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public EnumStatus getStatus() {
        return status;
    }

    public void setStatus(EnumStatus status) {
        this.status = status;
    }

    public GatewayEntity getGateway() {
        return gateway;
    }

    public void setGateway(GatewayEntity gateway) {
        this.gateway = gateway;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PeripheralDeviceEntity that = (PeripheralDeviceEntity) o;

        return id == that.id && (vendor != null ? vendor.equals(that.vendor) : that.vendor == null) && (dateCreated != null ? dateCreated.equals(that.dateCreated) : that.dateCreated == null) && (status != null ? status.equals(that.status) : that.status == null) && (gateway != null ? gateway.equals(that.gateway) : that.gateway == null);
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + (vendor != null ? vendor.hashCode() : 0);
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (gateway != null ? gateway.hashCode() : 0);
        return result;
    }
}

package service.gateways.services;

import org.springframework.stereotype.Service;
import service.gateways.entities.PeripheralDeviceEntity;
import service.gateways.exceptions.*;

import java.util.List;

@Service
public interface IPeripheralDeviceService {
    PeripheralDeviceEntity save(String gatewayId, PeripheralDeviceEntity peripheralDeviceRequest) throws ResourceNotFoundException, ExceedMaxDevicesException;

    void update(String gatewayId, Integer peripheralDeviceId, PeripheralDeviceEntity peripheralDeviceRequest) throws ResourceNotFoundException;

    void delete(String gatewayId, Integer peripheralDeviceId) throws ResourceNotFoundException;

    List<PeripheralDeviceEntity> findAllByGatewayId(String gatewayId) throws ResourceNotFoundException;
}

package service.gateways.services;

import org.springframework.stereotype.Service;
import service.gateways.entities.GatewayEntity;
import service.gateways.exceptions.AddressIpv4Exception;
import service.gateways.exceptions.ResourceExistsException;
import service.gateways.exceptions.ResourceNotFoundException;

import java.util.List;

@Service
public interface IGatewayService {
    GatewayEntity save(GatewayEntity gatewayRequest) throws ResourceExistsException, AddressIpv4Exception;

    void update(String gatewayId, GatewayEntity gatewayRequest) throws AddressIpv4Exception, ResourceNotFoundException;

    void delete(String gatewayId) throws ResourceNotFoundException;

    List<GatewayEntity> findAll();

    GatewayEntity findById(String gatewayId) throws ResourceNotFoundException;
}
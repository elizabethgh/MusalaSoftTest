package service.gateways.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.gateways.entities.GatewayEntity;
import service.gateways.exceptions.AddressIpv4Exception;
import service.gateways.exceptions.ResourceExistsException;
import service.gateways.exceptions.ResourceNotFoundException;
import service.gateways.repositories.IGatewayRepository;
import service.gateways.services.IGatewayService;

import java.util.List;
import java.util.Optional;

import static service.gateways.messages.Messages.*;

@Service
public class GatewayService implements IGatewayService {

    private IGatewayRepository gatewayRepository;

    @Autowired
    public GatewayService(IGatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    @Override
    public GatewayEntity save(GatewayEntity gatewayRequest) throws ResourceExistsException, AddressIpv4Exception {
        Optional<GatewayEntity> byId = gatewayRepository.findById(gatewayRequest.getId());
        if(byId.isPresent()) {
            throw new ResourceExistsException(message_gateway_exists);
        }

        if(!gatewayRequest.isValidAddressIpv4()) {
            throw new AddressIpv4Exception(message_address_not_valid);
        }

        return gatewayRepository.save(gatewayRequest);
    }

    @Override
    public void update(String gatewayId, GatewayEntity gatewayRequest) throws ResourceNotFoundException, AddressIpv4Exception {
        Optional<GatewayEntity> byId = gatewayRepository.findById(gatewayId);

        if(!byId.isPresent()) {
            throw new ResourceNotFoundException(String.format(message_gateway_not_found,gatewayId));
        }

        if (!gatewayRequest.isValidAddressIpv4()) {
            throw new AddressIpv4Exception(message_address_not_valid);
        }

        GatewayEntity gatewayEntity = byId.get();
        gatewayEntity.setHumanReadableName(gatewayRequest.getHumanReadableName());
        gatewayEntity.setIpv4Address(gatewayRequest.getIpv4Address());
        gatewayRepository.save(gatewayEntity);
     }

    @Override
    public void delete(String gatewayId) throws ResourceNotFoundException {
        Optional<GatewayEntity> byId = gatewayRepository.findById(gatewayId);

        if(!byId.isPresent()) {
            throw new ResourceNotFoundException(String.format(message_gateway_not_found, gatewayId));
        }

        gatewayRepository.deleteById(gatewayId);
    }

    @Override
    public List<GatewayEntity> findAll() {
        return gatewayRepository.findAll();
    }

    @Override
    public GatewayEntity findById(String gatewayId) throws ResourceNotFoundException {
        return gatewayRepository.findById(gatewayId).orElseThrow(() -> new ResourceNotFoundException(String.format(message_gateway_not_found, gatewayId)));
    }
}

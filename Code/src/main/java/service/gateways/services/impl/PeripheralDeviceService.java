package service.gateways.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.gateways.entities.GatewayEntity;
import service.gateways.entities.PeripheralDeviceEntity;
import service.gateways.exceptions.*;
import service.gateways.repositories.IGatewayRepository;
import service.gateways.repositories.IPeripheralDeviceRepository;
import service.gateways.services.IPeripheralDeviceService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static service.gateways.messages.Messages.*;

@Service
public class PeripheralDeviceService implements IPeripheralDeviceService {

    private final IPeripheralDeviceRepository peripheralDeviceRepository;

    private final IGatewayRepository gatewayRepository;

    @Autowired
    public PeripheralDeviceService(IGatewayRepository gatewayRepository, IPeripheralDeviceRepository peripheralDeviceRepository) {
        this.peripheralDeviceRepository = peripheralDeviceRepository;
        this.gatewayRepository = gatewayRepository;
    }

    @Override
    public PeripheralDeviceEntity save(String gatewayId, PeripheralDeviceEntity peripheralDeviceRequest) throws ResourceNotFoundException, ExceedMaxDevicesException {
        Optional<GatewayEntity> byId = gatewayRepository.findById(gatewayId);
        if (!byId.isPresent()) {
            throw new ResourceNotFoundException(String.format(message_gateway_not_found, gatewayId));
        }

        GatewayEntity gatewayExist = byId.get();
        if (gatewayExist.getPeripheralDevices().size() >= max_peripheral_devices_count) {
            throw new ExceedMaxDevicesException(String.format(message_peripheral_devices_count, max_peripheral_devices_count));
        }

        peripheralDeviceRequest.setGateway(gatewayExist);
        peripheralDeviceRequest.setDateCreated(LocalDateTime.now());
        return peripheralDeviceRepository.save(peripheralDeviceRequest);
    }

    @Override
    public void update(String gatewayId, Integer peripheralDeviceId, PeripheralDeviceEntity peripheralDeviceRequest) throws ResourceNotFoundException {
        if (!gatewayRepository.existsById(gatewayId)) {
            throw new ResourceNotFoundException(String.format(message_gateway_not_found, gatewayId));
        }

        Optional<PeripheralDeviceEntity> byId = peripheralDeviceRepository.findById(peripheralDeviceId);
        if(!byId.isPresent()){
            throw new ResourceNotFoundException(String.format(message_peripheral_device_not_found, peripheralDeviceId));
        }

        if(!byId.get().getGateway().getId().equals(gatewayId)) {
            throw new ResourceNotFoundException(message_peripheral_devices_has_not_the_same_gateway);
        }

        PeripheralDeviceEntity peripheralDevice = byId.get();
        peripheralDevice.setStatus(peripheralDeviceRequest.getStatus());
        peripheralDevice.setVendor(peripheralDeviceRequest.getVendor());

        peripheralDeviceRepository.save(peripheralDevice);
    }

    @Override
    public void delete(String gatewayId, Integer peripheralDeviceId) throws ResourceNotFoundException {
        if (!gatewayRepository.existsById(gatewayId)) {
            throw new ResourceNotFoundException(String.format(message_gateway_not_found, gatewayId));
        }

        Optional<PeripheralDeviceEntity> byId = peripheralDeviceRepository.findById(peripheralDeviceId);
        if (!byId.isPresent()) {
            throw new ResourceNotFoundException(String.format(message_peripheral_device_not_found, peripheralDeviceId));
        }

        if(!byId.get().getGateway().getId().equals(gatewayId)) {
            throw new ResourceNotFoundException(message_peripheral_devices_has_not_the_same_gateway);
        }

        peripheralDeviceRepository.delete(byId.get());
    }

    @Override
    public List<PeripheralDeviceEntity> findAllByGatewayId(String gatewayId) throws ResourceNotFoundException {
        if (!gatewayRepository.findById(gatewayId).isPresent()) {
            throw new ResourceNotFoundException(String.format(message_gateway_not_found, gatewayId));
        }

        return peripheralDeviceRepository.findByGatewayId(gatewayId);
    }
}

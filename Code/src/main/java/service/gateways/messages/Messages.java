package service.gateways.messages;

public class Messages {
    public final static String message_gateway_not_found = "Gateway Id %s not found";
    public final static String message_peripheral_device_not_found = "Peripheral Device Id %s not found";
    public final static String message_address_not_valid = "The value of address ipv4 is not valid.";
    public final static String message_gateway_exists = "The value of Gateway Id already exists.";
    public final static Integer max_peripheral_devices_count = 10;
    public final static String message_peripheral_devices_count = "The gateway can only to be %d peripheral devices.";
    public final static String message_peripheral_devices_has_not_the_same_gateway = "The Peripheral Device has a different Gateway Id.";
}
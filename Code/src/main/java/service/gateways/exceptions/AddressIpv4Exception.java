package service.gateways.exceptions;

public class AddressIpv4Exception extends Exception {
    public AddressIpv4Exception(String message){
        super(message);
    }
}
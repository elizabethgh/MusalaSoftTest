package service.gateways.exceptions;

public class ExceedMaxDevicesException extends Exception {
    public ExceedMaxDevicesException(String message){
        super(message);
    }
}

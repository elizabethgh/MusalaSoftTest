package service.gateways.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import service.gateways.entities.GatewayEntity;

@Repository
public interface IGatewayRepository extends JpaRepository<GatewayEntity, String> {

}


package service.gateways.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import service.gateways.entities.PeripheralDeviceEntity;

import java.util.List;

@Repository
public interface IPeripheralDeviceRepository extends JpaRepository<PeripheralDeviceEntity, Integer> {
    List<PeripheralDeviceEntity> findByGatewayId(String gatewayId);
}

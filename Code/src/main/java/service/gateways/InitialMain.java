package service.gateways;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("service.gateways.entities")
@EnableJpaRepositories("service.gateways.repositories")
@ComponentScan
public class InitialMain {
    public static void main(String[] args) {
        SpringApplication.run(InitialMain.class, args);
    }
}